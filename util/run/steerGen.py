def replaceArguments(runLines, nevents, externalLHEProducer):

    if externalLHEProducer:
        runLines = runLines.replace("[step]", "LHE,GEN")

    else:
        runLines = runLines.replace("[step]", "GEN")

    runLines = runLines.replace("[eventcontent]", "RAWSIM")
    runLines = runLines.replace("[nevents]", nevents)

    return runLines

def steer(runLines, nevents, externalLHEProducer):

    runLines += "cmsDriver.py Configuration/GenProduction/python/fragment.py --eventcontent [eventcontent] --mc --conditions auto:mc --step [step] --python_filename run.py --no_exec -n [nevents] --fileout file:genVal.root --customise Configuration/GenProduction/rivet.rivet --customise Configuration/GenProduction/random.random --customise Configuration/DataProcessing/Utils.addMonitoring\n"
    runLines = replaceArguments(runLines, nevents, externalLHEProducer)

    return runLines

