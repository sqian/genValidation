def replaceArguments(fragmentLines, gridpack, nevents):

    fragmentLines = fragmentLines.replace("[gridpack]", gridpack)
    fragmentLines = fragmentLines.replace("[nevents]", nevents)

    return fragmentLines

def steer(fragmentLines, gridpack, nevents):

    fragmentLines = replaceArguments(fragmentLines, gridpack, nevents)
    return fragmentLines
