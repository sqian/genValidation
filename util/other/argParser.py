import os
import sys
import argparse

def checkArguments(args):

    # check MC event generators
    mcegs = ["madgraph", "powheg", "sherpa", "pythia", "herwig"]
    if not args.mceg in mcegs:
        sys.exit("[error] '-m' '--mceg' should be one of {0}".format(mcegs))

    # check MC event generators that necessarily need gridpacks
    gridpack_mcegs = ["madgraph", "powheg", "sherpa"]
    if args.gridpack:
        if not args.mceg in gridpack_mcegs:
            sys.exit("[error] '-m' '--mceg' should be one of {0} when using '-g' '--gridpack'".format(gridpack_mcegs))
        else:
            if not os.path.isabs(args.gridpack):
                args.gridpack = os.path.join(os.getcwd(), args.gridpack)

    # check showering fragments
    if not args.fragment:
        sys.exit("[error] '-f' '--fragment' should be given")

    # check beam energy to do rivet data analysis
    beames = ["7000", "8000", "13000", "13600", "14000"]
    if not args.beame in beames:
        sys.exit("[error] '-b' '--beame' should be one of : {0}".format(beames))

    # check output name and if it's not there give dataset name to the directory
    if not args.datasetname:
        sys.exit("[error] '-d' '--datasetname' should be given")
    else:
        args.output = args.datasetname

    # check njobs = int
    try:
        int(args.njobs)
    except:
        sys.exit("[error] '-j' '--njobs' is not an integer")

    # check nevents = int
    try:
        int(args.nevents)
    except:
        sys.exit("[error] '-n' '--nevents' is not an integer")

    # check output format
    keeps = [None, "GEN", "NanoGEN", "DQM"]
    if not args.keep in keeps:
        sys.exit("[error] '-k' '--keep' should be one of : {0}".format (keeps))

    # check output path
    if args.keeppath:
        if not os.path.exists(args.keeppath):
            sys.exit("[error] '-p' '--keeppath' does not exist")

    # check job scheduler type
    queues = ["condor", "cmsconnect", "pbs"]
    if not args.queue:
        import socket
        hostname = socket.gethostname()
        if "lxplus" in hostname:
            args.queue = "condor"
        elif hostname == "login-el7.uscms.org": # CMS connect
            args.queue = "cmsconnect"
        else:
            if not args.queue in queues:
                sys.exit("[error] '-q' '--queue' should be one of : {0}".format(queues))

    try:
        int(args.lhaid)
    except:
        sys.exit("[error] '--lhaid' is not an integer")

    return args


def parseArguments():

    parser = argparse.ArgumentParser("Generator validation scripts")

    # argument flags
    parser.add_argument("-m", "--mceg",\
                        action="store", dest="mceg", default=None,\
                        help = "generator to validate")

    parser.add_argument("-g", "--gridpack",\
                        action="store", dest="gridpack", default=None,\
                        help = "gridpack to run with")

    parser.add_argument("-f", "--fragment",\
                        action="store", dest="fragment", default=None,\
                        help = "hadronizer to run with")

    parser.add_argument("-a", "--analysis",\
                        action="store", dest="analysis", default=None,\
                        help = "rivet analysis to analyze")

    parser.add_argument("-b", "--beame",\
                        action="store", dest="beame", default=None,\
                        help = "do data rivet analysis with same beam energy")

    parser.add_argument("-d", "--datasetname",\
                        action="store", dest="datasetname", default=None,\
                        help = "dataset name")

    parser.add_argument("-n", "--nevents",\
                        action="store", dest="nevents", default="5000",\
                        help = "number of events to submit per job")

    parser.add_argument("-j", "--njobs",\
                        action="store", dest="njobs", default="1",\
                        help = "number of jobs to submit")

    parser.add_argument("-o", "--output",\
                        action="store", dest="output", default=None,\
                        help = "name of output directory")

    parser.add_argument("-k", "--keep",\
                        action="store", dest="keep", default=None,\
                        help = "store output ROOT file format")

    parser.add_argument("-p", "--keeppath",\
                        action="store", dest="keeppath", default=None,\
                        help = "path to store output ROOT file, necessarily need '--keep'")

    parser.add_argument("-q", "--queue",\
                        action="store", dest="queue", default=None,\
                        help = "type of job scheduler")

    parser.add_argument("--lhaid",\
                        action="store", dest="lhaid", default="325300",\
                        help = "reference PDF set")

    return parser.parse_args()

