import os

def getAnalysisNames(analysis, beame):

    analysisList = []

    # default Rivet MC analysis
    default_mc = os.path.join("config", "analysis", "default", "MC.dat")
    ll = open(default_mc, encoding="utf-8").readlines()
    for l in ll:
        l = l.strip().split("#")[0]
        exec("analysisList.append(\'{0}\')".format(l, beame), locals())

    if analysis:
        # physics process Rivet MC analysis
        analysis_mc = os.path.join("config", "analysis", analysis, "MC.dat")
        ll = open(analysis_mc, encoding="utf-8").readlines()
        for l in ll:
            l = l.strip().split("#")[0]
            exec("analysisList.append(\'{0}\')".format(l, beame), locals())

        analysis_beame = os.path.join("config", "analysis", analysis, "{0}.dat".format(beame))
        if os.path.exists(analysis_beame):
            ll = open(analysis_beame, encoding="utf-8").readlines()
            for l in ll:
                l = l.strip().split("#")[0].replace(" ", "").replace("\t", "")
                exec("analysisList.append(\'{0}\')".format(l, beame), locals())

    analysisNames = ""
    for a in analysisList:
        if a.startswith("MC_"):
            a = "{0}:ENERGY={1}".format(a, beame)
        analysisNames += "\'{0}\',".format(a)

    analysisNames = analysisNames[:-1]

    return analysisNames

def getAnalysis(args):

    analysisLines = ""
    analysisLines += """
import FWCore.ParameterSet.Config as cms

def rivet(process):
\tprocess.load('GeneratorInterface.RivetInterface.rivetAnalyzer_cfi')
\tprocess.rivetAnalyzer.AnalysisNames = cms.vstring([analysisnames])
\tprocess.rivetAnalyzer.OutputFile = cms.string('rivet_result.yoda')
\tprocess.rivetAnalyzer.CrossSection = cms.double(1.)
\tprocess.rivetAnalyzer.UseExternalWeight = cms.bool(True)
\tprocess.rivetAnalyzer.useLHEweights = cms.bool(True)
\tprocess.rivetAnalyzer.selectMultiWeights = cms.string('.*MUR.*MUF.*')
\tprocess.generation_step+=process.rivetAnalyzer

\treturn(process) 
"""

    analysisNames = getAnalysisNames(args.analysis, args.beame)

    analysisLines = analysisLines.replace("[analysisnames]", analysisNames)
    analysisLines = analysisLines.replace("[lhaid]", args.lhaid)

    return analysisLines


def main(args, runpointDir):

    analysisLines = getAnalysis(args)
    analysisFile = open("{0}".format(os.path.join(runpointDir, "rivet.py")), "w")
    analysisFile.write(analysisLines)
    analysisFile.close()


