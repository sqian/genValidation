import os
import sys

from .steerCondor import steer as steerCondor
from .steerCMSConnect import steer as steerCMSConnect

def getSubmit(args, runpointDir):

    submitLines = ""

    if args.queue == "condor":
        submitLines = steerCondor(submitLines, args.datasetname, runpointDir)
    elif args.queue == "cmsconnect":
        submitLines = steerCMSConnect(submitLines, args.gridpack, args.datasetname, runpointDir)
    else:
        sys.exit("[error] getSubmit unknown")

#    elif args.queue == "pbs": #TODO
#        pass

    return submitLines
