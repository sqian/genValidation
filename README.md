# genValidation

## Get the repo
```bash
git clone https://gitlab.cern.ch/jixiao/genValidation.git
cd genValidation
```

## Set the environment
```bash
source setup.sh
```

## Use `genval-run` to generate yoda files

Following are some examples for generator validation tasks.

<details>
<summary>WJets mlm jetbinned</summary>

Please remember to change the path of the `gridpack`.

```bash
# test with some minimal events
## generate configs
./genval-run \
-g /home/jiexiao/public/genval/gps/W0JetsToLNu_mlm_slc7_amd64_gcc900_CMSSW_12_0_2_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py \
-n 100 -j 10 -d W0Jet_MLM -q cmsconnect -m madgraph -b 13000 -a W
## submit jobs
# rm -rf W0Jet_MLM


# ==============> START submitting jobs for validation

#######################################################
####################### MG v331 #######################
#######################################################
# W0Jet
## generate configs
./genval-run \
-g /home/jiexiao/public/genval/gps/W0JetsToLNu_mlm_slc7_amd64_gcc900_CMSSW_12_0_2_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py \
-n 10240 -j 599 -d W0Jet_MLM_331 -q cmsconnect -m madgraph -b 13000 -a W

# W1Jet
## generate configs
./genval-run \
-g /home/jiexiao/public/genval/gps/W1JetsToLNu_mlm_slc7_amd64_gcc900_CMSSW_12_0_2_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py \
-n 10240 -j 699 -d W1Jet_MLM_331 -q cmsconnect -m madgraph -b 13000 -a W

# W2Jet
## generate configs
./genval-run \
-g /home/jiexiao/public/genval/gps/W2JetsToLNu_mlm_slc7_amd64_gcc900_CMSSW_12_0_2_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py \
-n 10240 -j 799 -d W2Jet_MLM_331 -q cmsconnect -m madgraph -b 13000 -a W

# W3Jet
## generate configs
./genval-run \
-g /home/jiexiao/public/genval/gps/W3JetsToLNu_mlm_slc7_amd64_gcc900_CMSSW_12_0_2_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py \
-n 10240 -j 899 -d W3Jet_MLM_331 -q cmsconnect -m madgraph -b 13000 -a W

# W4Jet
## generate configs
./genval-run \
-g /home/jiexiao/public/genval/gps/W4JetsToLNu_mlm_slc7_amd64_gcc900_CMSSW_12_0_2_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py \
-n 10240 -j 999 -d W4Jet_MLM_331 -q cmsconnect -m madgraph -b 13000 -a W

#######################################################
####################### MG v273 #######################
#######################################################
# W0Jet
## generate configs
./genval-run \
-g /home/jiexiao/public/genval/gps/mg273_W0JetsToLNu_mlm_slc7_amd64_gcc630_CMSSW_9_3_16_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py \
-n 10240 -j 599 -d W0Jet_MLM_273 -q cmsconnect -m madgraph -b 13000 -a W

# W1Jet
## generate configs
./genval-run \
-g /home/jiexiao/public/genval/gps/mg273_W1JetsToLNu_mlm_slc7_amd64_gcc630_CMSSW_9_3_16_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py \
-n 10240 -j 699 -d W1Jet_MLM_273 -q cmsconnect -m madgraph -b 13000 -a W

# W2Jet
## generate configs
./genval-run \
-g /home/jiexiao/public/genval/gps/mg273_W2JetsToLNu_mlm_slc7_amd64_gcc630_CMSSW_9_3_16_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py \
-n 10240 -j 799 -d W2Jet_MLM_273 -q cmsconnect -m madgraph -b 13000 -a W

# W3Jet
## generate configs
./genval-run \
-g /home/jiexiao/public/genval/gps/mg273_W3JetsToLNu_mlm_slc7_amd64_gcc630_CMSSW_9_3_16_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py \
-n 10240 -j 899 -d W3Jet_MLM_273 -q cmsconnect -m madgraph -b 13000 -a W

# W4Jet
## generate configs
./genval-run \
-g /home/jiexiao/public/genval/gps/mg273_W4JetsToLNu_mlm_slc7_amd64_gcc630_CMSSW_9_3_16_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py \
-n 10240 -j 999 -d W4Jet_MLM_273 -q cmsconnect -m madgraph -b 13000 -a W

```
</details>

<details>
<summary>WJets fxfx jetbinned</summary>

Please remember to change the path of the `gridpack`.

```bash
#######################################################
####################### MG v331 #######################
#######################################################
# W0Jet
## generate configs
./genval-run \
-g /home/jiexiao/public/genval/gps/wellnu0j_5f_NLO_FXFX_slc7_amd64_gcc900_CMSSW_12_0_2_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_aMCatNLO_FXFX_5f_max2j_LHE_pythia8_cff.py \
-n 10240 -j 799 -d W0Jet_FXFX_331 -q cmsconnect -m madgraph -b 13000 -a W

# W1Jet
## generate configs
./genval-run \
-g /home/jiexiao/public/genval/gps/wellnu1j_5f_NLO_FXFX_slc7_amd64_gcc900_CMSSW_12_0_2_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_aMCatNLO_FXFX_5f_max2j_LHE_pythia8_cff.py \
-n 10240 -j 899 -d W1Jet_FXFX_331 -q cmsconnect -m madgraph -b 13000 -a W

# W2Jet
## generate configs
./genval-run \
-g /home/jiexiao/public/genval/gps/wellnu2j_5f_NLO_FXFX_slc7_amd64_gcc900_CMSSW_12_0_2_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_aMCatNLO_FXFX_5f_max2j_LHE_pythia8_cff.py \
-n 10240 -j 999 -d W2Jet_FXFX_331 -q cmsconnect -m madgraph -b 13000 -a W

#######################################################
####################### MG v273 #######################
#######################################################
# W0Jet
## generate configs
./genval-run \
-g /home/jiexiao/public/genval/gps/wellnu0j_5f_NLO_FXFX_slc7_amd64_gcc700_CMSSW_10_6_19_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_aMCatNLO_FXFX_5f_max2j_LHE_pythia8_cff.py \
-n 10240 -j 799 -d W0Jet_FXFX_273 -q cmsconnect -m madgraph -b 13000 -a W

# W1Jet
## generate configs
./genval-run \
-g /home/jiexiao/public/genval/gps/wellnu1j_5f_NLO_FXFX_slc7_amd64_gcc700_CMSSW_10_6_19_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_aMCatNLO_FXFX_5f_max2j_LHE_pythia8_cff.py \
-n 10240 -j 899 -d W1Jet_FXFX_273 -q cmsconnect -m madgraph -b 13000 -a W

# W2Jet
## generate configs
./genval-run \
-g /home/jiexiao/public/genval/gps/wellnu2j_5f_NLO_FXFX_slc7_amd64_gcc700_CMSSW_10_6_19_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_aMCatNLO_FXFX_5f_max2j_LHE_pythia8_cff.py \
-n 10240 -j 999 -d W2Jet_FXFX_273 -q cmsconnect -m madgraph -b 13000 -a W

```

</details>

<details>
<summary>DYJets mlm inclusive</summary>

Please remember to change the path of the `gridpack`.

```bash
# DY01234J MLM
#######################################################
####################### MG v331 #######################
#######################################################
./genval-run \
-g /home/jiexiao/public/genval/gps/dyellell01234j_5f_LO_MLM_slc7_amd64_gcc900_CMSSW_12_0_2_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py \
-n 10240 -j 999 -d DY01234J_MLM_331 -q cmsconnect -m madgraph -b 13000 -a Z
#######################################################
####################### MG v265 #######################
#######################################################
./genval-run \
-g /cvmfs/cms.cern.ch/phys_generator/gridpacks/2017/13TeV/madgraph/GenValidation/V265_vs_V273/DYMLM_mll50/V5_2.6.5/dyellell_5f_LO_MLM_slc7_amd64_gcc700_CMSSW_10_6_19_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py \
-n 10240 -j 999 -d DY01234J_MLM_273 -q cmsconnect -m madgraph -b 13000 -a Z
```

</details>

<details>
<summary>DYJets mlm htbinned</summary>

Please remember to change the path of the `gridpack`.

```bash
# DYJets MLM
#######################################################
####################### MG v331 #######################
#######################################################
# DYJets_HT-0to40
./genval-run \
-g /local-scratch/jiexiao/gps/DYJets_HT-0to40_slc7_amd64_gcc900_CMSSW_12_0_2_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py \
-n 10240 -j 599 -d DYJets_HT-0to40 -q cmsconnect -m madgraph -b 13000 -a Z
# DYJets_HT-70to100
./genval-run \
-g /local-scratch/jiexiao/gps/DYJets_HT-70to100_slc7_amd64_gcc900_CMSSW_12_0_2_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py \
-n 10240 -j 599 -d DYJets_HT-70to100 -q cmsconnect -m madgraph -b 13000 -a Z
# DYJets_HT-100to200
./genval-run \
-g /local-scratch/jiexiao/gps/DYJets_HT-100to200_slc7_amd64_gcc900_CMSSW_12_0_2_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py \
-n 10240 -j 599 -d DYJets_HT-100to200 -q cmsconnect -m madgraph -b 13000 -a Z
# DYJets_HT-200to400
./genval-run \
-g /local-scratch/jiexiao/gps/DYJets_HT-200to400_slc7_amd64_gcc900_CMSSW_12_0_2_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py \
-n 10240 -j 599 -d DYJets_HT-200to400 -q cmsconnect -m madgraph -b 13000 -a Z
# DYJets_HT-400to600
./genval-run \
-g /local-scratch/jiexiao/gps/DYJets_HT-400to600_slc7_amd64_gcc900_CMSSW_12_0_2_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py \
-n 10240 -j 599 -d DYJets_HT-400to600 -q cmsconnect -m madgraph -b 13000 -a Z
# DYJets_HT-600to800
./genval-run \
-g /local-scratch/jiexiao/gps/DYJets_HT-600to800_slc7_amd64_gcc900_CMSSW_12_0_2_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py \
-n 10240 -j 599 -d DYJets_HT-600to800 -q cmsconnect -m madgraph -b 13000 -a Z
# DYJets_HT-800to1200
./genval-run \
-g /local-scratch/jiexiao/gps/DYJets_HT-800to1200_slc7_amd64_gcc900_CMSSW_12_0_2_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max4j_qCut19_LHE_pythia8_cff.py \
-n 10240 -j 599 -d DYJets_HT-800to1200 -q cmsconnect -m madgraph -b 13000 -a Z
```

</details>

## Use `genval-grid` to merge the yoda files

Direcly append the folder name of the process run with `genval-run`,
e.g., 

```bash
./genval-grid DYJets_HT-800to1200
```

>Tips: remember to set the CMSSW enviranment when you run `genval-grid`

## Draw comparison plots

Use `singularity` to run `rivet` commands, to initialize, run

```bash
singularity exec -B $PWD:$PWD docker://hepstore/rivet rivet-mkhtml --help
```

An example to compare two samples, the scale uncertainty is considered,

```bash
rivet-mkhtml --errs \
W0Jet_MLM_331.yoda:Title=W0Jet_MLM_331:Variations=.*MUR.*MUF.*:BandComponentEnv=.*MUR.*MUF.* \
W0Jet_MLM_273.yoda:Title=W0Jet_MLM_273:Variations=.*MUR.*MUF.*:BandComponentEnv=.*MUR.*MUF.* \
--remove-options -m MC_ -o output_W0Jet
```

>Tips: use `alias` to make the commands simpler, e.g.,
    
    #!/bin/bash

    alias rivet='singularity exec -B $PWD:$PWD docker://hepstore/rivet rivet'
    alias rivet-mkhtml='singularity exec -B $PWD:$PWD docker://hepstore/rivet rivet-mkhtml'
    alias rivet-mkanalysis='singularity exec -B $PWD:$PWD docker://hepstore/rivet rivet-mkanalysis'
    alias rivet-build='singularity exec -B $PWD:$PWD docker://hepstore/rivet rivet-build'
    alias rivet-merge='singularity exec -B $PWD:$PWD docker://hepstore/rivet rivet-merge'
    alias rivet-cmphistos='singularity exec -B $PWD:$PWD docker://hepstore/rivet rivet-cmphistos'
    alias make-plots='singularity exec -B $PWD:$PWD docker://hepstore/rivet make-plots'
    alias compare-histos='singularity exec -B $PWD:$PWD docker://hepstore/rivet compare-histos'
    alias make-pgfplots='singularity exec -B $PWD:$PWD docker://hepstore/rivet make-pgfplots'
    alias make-plots-fast='singularity exec -B $PWD:$PWD docker://hepstore/rivet make-plots-fast'

    
